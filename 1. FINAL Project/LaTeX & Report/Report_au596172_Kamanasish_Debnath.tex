\documentclass[10pt, a4paper]{article}

% Pretty much all of the ams maths packages
\usepackage{amsmath,amsthm,amssymb,amsfonts}

% Allows you to manipulate the page a bit
\usepackage[a4paper]{geometry}

% Pulls the page out a bit - makes it look better (in my opinion)
\usepackage{a4wide}

% Removes paragraph indentation (not needed most of the time now)
\usepackage{parskip}

% Allows inclusion of graphics easily and configurably
\usepackage{graphicx}

% Provides ways to make nice looking tables
\usepackage{booktabs}

% Allows you to rotate tables and figures
\usepackage{rotating}

% Allows shading of table cells
\usepackage{colortbl}
% Define a simple command to use at the start of a table row to make it have a shaded background
\newcommand{\gray}{\rowcolor[gray]{.9}}

\usepackage{textcomp}

% Provides commands to make subfigures (figures with (a), (b) and (c))
\usepackage{subfigure}

% Typesets URLs sensibly - with tt font, clickable in PDFs, and not breaking across lines
\usepackage{url}

% Makes references hyperlinks in PDF output
\usepackage{hyperref}

% Provides ways to include syntax-highlighted source code
\usepackage{listings}
\lstset{frame=single, basicstyle=\ttfamily}

% Provides Harvard-style referencing
\usepackage{natbib}
\bibpunct{(}{)}{;}{a}{,}{,}

% Provides good access to colours
\usepackage{color}
\usepackage{xcolor}


% Allows fancy stuff in the page header
\usepackage{fancyhdr}
\pagestyle{fancy}

% Vastly improves the standard formatting of captions
\usepackage[margin=10pt,font=small,labelfont=bf, labelsep=endash]{caption}

% Standard title, author etc.
\title{Using $4^{th}$ order Runge-Kutta Method for studying Superradiance}
\author{Kamanasish Debnath \\AU ID: au596172 \\PhD student \\ Aarhus University}
\date{}

% Put text on the left-hand and right-hand side of the header
\fancyhead{}
\lhead{Cource code: 225172U011}
\rhead{Final Report}
\chead{}


\begin{document}
\maketitle
\section{Introduction}
In numerical analysis, one of the most widely used iterative algorithm for solving ordinary differential equations (ODE) is the Runge-Kutta method, which is an advanced formulation of the first order Euler method. The entire family of Runge-Kutta methods consists of many algorithms depending on the accuracy like the First order Runge-Kutta, Second order Runge-Kutta and so on. This family of iterative methods was first developed around 1900 by German Mathematicians C. Runge and M. W. Kutta. The $4^{th}$ order Runge-Kutta method (RK4) is considered the most stable and efficiant in handling majority of the problems ane hence we use this algorithm to solve a system of equations arising from a realistic physical problem. The report is arranged as follows. In Sec. I, we briefly discuss the algorithm of $4^{th}$ order Runge-Kutta method. In Sec. II, we introduce the mathematics of the problem we wish to solve using this technique and finally present the results in Sec. III including some comments and validity of the results.
\section{RK4 algorithm}
This section is based on the details provided by Wikipedia. Let us consider that the particular ODE that we wish to solve can be written as- 
\begin{equation}
\frac{dy(t)}{dt}= y'(t)= f(y(t),t)
\end{equation}
with some initial condition $y(t_i)= y_0$. We wish to integrate the equations starting from some initial time $t_i$ till final time $t_f$ at a step size of $h>0$. Choosing a correct step size is crucial and plays a major role in determining the accuracy of the solutions. Starting from $y(t_i)= y_0$, the subsequent steps $y_{n+1}$ can be calculated as- 
\begin{equation}
y_{n+1} = y_n + (1/6)*(k_1 + 2k_2 + 2k_3 + k_4)
\end{equation}
where $t_{n+1} = t_n + h$, for $n= 0,1,2..$ and so on using the following relations-
\begin{equation}
\begin{array}{lll}
k_1 &=& h f(t_n, y_n) \\
k_2 &=& h f(t_n + h/2, y_n + k_1/2) \\
k_3 &=& h f(t_n + h/2, y_n + k_2/2) \\
k_4 &=& h f(t_n + h, y_n + k_3) 
\end{array}
\end{equation}
Here $y_{n+1}$ is the RK4 approximation of $y(t_{n+1})$, and the next value $y_{n + 1}$ is determined by the present value $y_n$ plus the weighted average of four increments, where each increment is the product of the size of the interval, $h$, and an estimated slope specified by function $f$ on the right-hand side of the differential equation. Each of the slope estimates $k_n$, $n=1,2,3,4$ can be explained in the following manner- \newline
\begin{itemize}
   \item $k_1$ is the slope at the beginning of the time interval using $y$. This step is basically the Euler's method.
   \item $k_2$ is the increment based on the slope at the midpoint of the interval, using $y$ and $k_1$.
   \item $k_3$ is again the increment based on the slope at the midpoint, but now using $y$ and $K_2$.
   \item $k_4$ is the increment based on the slope at the end of the interval, using $y$ and $k_3$.
\end{itemize}
In averaging the four increments, greater weight is given to the increments at the midpoint. If $f$ is independent of $y$, so that the differential equation is equivalent to a simple integral, then RK4 is Simpson's rule. The RK4 method is a fourth-order method, meaning that the local truncation error is on the order of $\mathcal{O}(h^5)$ while the total accumumlated error is of the order of $\mathcal{O}(h^4)$. 
\section{Superradiance}
In this section, we discuss a quantum system formed by $N$ two-level atoms interacting with single electromagnetic radiation field. For simplicity, the decay rates of the atoms are negligible as compared to the damping of the optical cavity and has been neglected in the following treatment. The Hamiltonian of such a system can be written as ($\hbar= 1$ throughout)-
\begin{equation}
H= \omega_c a^{\dagger}a + \omega_a\sum_{i=1}^{N}\sigma_z + \sum_{i=1}^{N}g(a\sigma^{(1)}_+ + \sigma^{(i)}_-a^{\dagger})
\end{equation}
where $\omega_c, \omega_a, g$ are the frequency of optical mode, atomic transition frequency and single atom-cavity coupling. We start the dynamics of the system by putting all the atoms in the excited state and study how the atomic and photon population evolves over time. In the absence of any external pump and considering resonance condition ($\omega_c=\omega_a=0$), the dynamics of the system can be studied by considering only the following non zero terms in the equations of motion of the system operators (truncated to second order using Wick's theorem)- 
\begin{equation}
\dot{\langle aa^{\dagger}\rangle} = igN\langle a\sigma_+\rangle - igN\langle \sigma_-a^{\dagger}\rangle - \kappa(\langle aa^{\dagger}\rangle - 1)
\end{equation}
\begin{equation}
\dot{\langle\sigma_z\rangle} = -2ig\langle a\sigma_+\rangle + 2ig\langle \sigma_- a^{\dagger}\rangle
\end{equation}
\begin{equation}
\dot{\langle a\sigma_+\rangle} = -i(g/2)(1-\langle\sigma_z\rangle) - ig\langle\sigma_z\rangle\langle aa^{\dagger}\rangle - ig(N-1)\langle \sigma^1_1\sigma^2_+\rangle - \kappa/2\langle a\sigma_+\rangle
\end{equation}
\begin{equation}
\dot{\langle\sigma^1_1\sigma^2_+\rangle} = -ig\langle\sigma_z\rangle\langle \sigma_- a^{\dagger}\rangle + ig\langle\sigma_z\rangle\langle a\sigma_+\rangle
\end{equation}
We consider that the atoms are identical and the last equation takes into consideration the correlations between individual atoms.

\section{Modelling and Results}
In the last section, all the equations required to approximately understand the dynamics of $N$ atoms interacting with single optical mode were shown. Note that the terms are not necessarily real and hence the numeric integration has to be done by separating the real and imaginary parts of the equations. This leads to $6$ equations since atom excitation and photon population are known to be always real with no phase in the present settings and initial conditions. We consider $N=10^5, g= 0.5$ and in the absence of any cavity damping, we expect to observe the Rabi oscillations, showing the exchange of energy between atoms and photons. We get the same result and the plots for atom and photon occupation has been shown below.
\begin{figure}[h!]
\includegraphics[width=12cm]{photon_noloss.pdf}
\caption{The photon population as a function of time for $\kappa=0$}
\centering
\end{figure}
\begin{figure}[h!]
\includegraphics[width=12cm]{Sz_noloss.pdf}
\caption{The atom population as a function of time for $\kappa=0$}
\centering
\end{figure}
\textbf{Note that this is not the usual Rabi oscillation as observed in Jaynes-Cumming model which follows sinusoidal pattern. In the present setting, due to large number of atoms $N$, the atoms decay collectively leading to a superradiant peak in photon occupation. Hence the population pattern shown collective effect of $N$ atoms. This phenomenon is termed as Superradiance.}

Further, we studied the effect of cavity dissipation on the dynamics. In the presence of cavity dissipation, we expect the photon and atomic excitations to decay over time. However due to collective effect, if the photons emitted by the decay of the excited atoms stays in the cavity for long time, the enhanced coupling will ensure the reabsorption of atoms and will result in some oscillations before getting leaked out of the cavity. However, its already known and intuitive that in the presence of dissipation, the atom and photon population will go to the ground state, which is vacuum for photonic mode and ground state for atoms. We have the similar observation from the RK4 algorithm we have used. The plots has been shown below.  
\begin{figure}[h!]
\includegraphics[width=12cm]{photon_loss.pdf}
\caption{The photon population as a function of time for $\kappa=0.5$}
\centering
\end{figure}
\begin{figure}[h!]
\includegraphics[width=12cm]{Sz_loss.pdf}
\caption{The atom population as a function of time for $\kappa=0.5$}
\centering
\end{figure}

\section{Conclusion}
In conclusion, we have shown the collective enhanced Rabi oscillations when an ensemble of homogeneous atoms interact with a single optical mode. We have derived the Heisenberg's equations of motion and truncated to second order using Wick's theorem. The equations were solved using $4^{th}$ order Runge-Kutta method to plot the system observables like the atomic and photon excitation. The results match with our expectation and also match with the preliminary results I have been calculating using Matlab in my PhD project. This confirms the validity of the algorithm. Moreover, the fact that the atomic and photon excitations decay to the ground state in the presence of dissipation over time consolidates the fact that the algorithm has been incorporated correctly.


\section{Reference}
\begin{itemize}
\item WIKIPEDIA- Runge-Kutta Methods
\item Science Advances  Vol. 2, No. 10, e1601231 (2016). (Experimental observation of collective enhanced Rabi oscillations using Strontium atoms).
\item Phys. Rev. X 6, 011025 (2016). - (Superradiance using ultra-cold atoms)
\end{itemize}








\end{document}


