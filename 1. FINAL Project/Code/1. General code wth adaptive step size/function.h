#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>


void ode45_onestep(gsl_matrix* A, double dt, double t);
void ode45_twostep(gsl_matrix* A, double dt, double t);
double fx(double t, double y);
