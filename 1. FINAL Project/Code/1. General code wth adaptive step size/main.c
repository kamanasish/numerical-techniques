/* This is a general program that solves ODE using 4th order Runge Kutta Method
--------------------------------------------------------------------------------------------------------

Name	:	KAMANASISH DEBNATH (PhD student- 1st year)
AU ID	: 	au596172
Course	: 	Practical Programming and Numerical Methods


-------------------------------------------------------------------------------------------------------- */


#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"function.h"


int main(void)
{
int N= 1;							// Number of equations to be solved
gsl_matrix * A = gsl_matrix_alloc(N, 3);			// Matrix to store instantaneous values of integration
double t_start= 0.0;						// Start time
double t_end= 3*(22.0/7);					// End time
double dt= 0.01;						// Step size
double accuracy= 0.001;						// Desired accuracy
gsl_matrix_set(A,0,0,0);					// Initial condition 
double t= t_start;					// Checks the progress of time
int counter= 1.0;

printf("Time		  y(t)		Error	        Step Size	Desired Accuracy\n");
printf("---------------------------------------------------------------------------------\n");
do
{
counter++;
ode45_onestep(A, dt, t);
ode45_twostep(A, dt, t);
double error= fabs(gsl_matrix_get(A,0,1) - gsl_matrix_get(A,0,2));

	while(error>accuracy)
		{
		dt=dt/2.0;
		ode45_onestep(A, dt, t);
		ode45_twostep(A, dt, t);
		error= fabs(gsl_matrix_get(A,0,1) - gsl_matrix_get(A,0,2));
		}

gsl_matrix_set(A,0,0,gsl_matrix_get(A,0,1));
printf("%1f  |	%1f  | 	%1f  |	%1f  |	%1f\n", t, gsl_matrix_get(A,0,1), error, dt, accuracy);
gsl_matrix_set(A,0,0,gsl_matrix_get(A,0,2));
t= t + dt;

}while(t<=t_end);


return(0);
}
