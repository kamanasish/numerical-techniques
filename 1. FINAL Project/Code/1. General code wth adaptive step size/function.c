#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>


// Write all the equations here 

double fx(double t, double y)
{
return(cos(t)*exp(t/2)*sin(t));
}



void ode45_onestep(gsl_matrix* A, double dt, double t)
{
double y= gsl_matrix_get(A,0,0);
double k1= dt * fx(t,y);
double k2= dt * fx(t+(dt/2.0),y);
double k3= dt * fx(t+(dt/2),y);
double k4= dt * fx(t+dt, y);
double result= y + ((1/6.0)*(k1 + (2*k2) + (2*k3) + k4));
gsl_matrix_set(A,0,1,result);
	
}





void ode45_twostep(gsl_matrix* A, double dt, double t)
{

dt= dt/2;
double y= gsl_matrix_get(A,0,0);
double k1= dt * fx(t,y);
double k2= dt * fx(t+(dt/2.0),y);
double k3= dt * fx(t+(dt/2),y);
double k4= dt * fx(t+dt, y);
double result= y + ((1/6.0)*(k1 + (2*k2) + (2*k3) + k4));

k1=0, k2= 0, k3=0, k4=0;


k1= dt * fx(t,y);
k2= dt * fx(t+(dt/2.0),y);
k3= dt * fx(t+(dt/2),y);
k4= dt * fx(t+dt, y);
result= y + ((1/6.0)*(k1 + (2*k2) + (2*k3) + k4));
gsl_matrix_set(A,0,2,result);
	}

