#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>



double EQ0(double ASp_imag, double AAd_real);
double EQ1(double Sz_real, double ASp_imag);
double EQ2(double SpSm_imag, double ASp_real);
double EQ3(double Sz_real, double AAd_real, double ASp_imag, double SpSm_real);
double EQ4(double Sz_real, double ASp_imag, double SpSm_imag);
double EQ5(double Sz_real, double ASp_real, double SpSm_real);
double ode45_onestep(gsl_matrix* A, double dt, int num_of_equations);
