/* This is a program that solves ODE using 4th order Runge Kutta Method



--------------------------------------------------------------------------------------------------------

Name	:	KAMANASISH DEBNATH (PhD student- 1st year)
AU ID	: 	au596172
Course	: 	Practical Programming and Numerical Methods


--------------------------------------------------------------------------------------------------------


Problem Solved: SUPERRADIANCE THEORY IN AN ENSEMBLE OF TWO LEVEL ATOMS 

References:
1. Science Advances  Vol. 2, No. 10, e1601231 (2016)
2. Phys. Rev. X 6, 011025 (2016)
--------------------------------------------------------------------------------------------------------*/





#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"function.h"


int main(void)
{


/*-------------------------------------  INTIAL DECLARATIONS  -------------------------------------------*/

int num_of_equations= 6;					// Number of ODE to be solved
double time_end = 20.0;						// End time
double dt= 0.00001;						// Initial step size		
gsl_matrix * A = gsl_matrix_alloc(num_of_equations, 3);		// Matrix to store updates.... twice because of separate real and imaginary part


 

/*-------------------------------------  INTIAL CONDITIONS  -------------------------------------------*/
gsl_matrix_set(A, 0, 0, 1.0);		// Real[<a a+>] = 1

gsl_matrix_set(A, 1, 0, 1.0);		// Real[<Sz>] = 1

gsl_matrix_set(A, 2, 0, 0.0);		// Real[<ASp>] = 0
gsl_matrix_set(A, 3, 0, 0.0);		// Imag[<ASp>] = 0

gsl_matrix_set(A, 4, 0, 0.0);		// Real[<Sp(1)Sm(2)>] = 0
gsl_matrix_set(A, 5, 0, 0.0);		// Imag[<Sp(1)Sm(2)>] = 0


/*-------------------------------------  MAIN DYNAMICS LOOP  -------------------------------------------*/
int counter= 1.0;
double t= 0.0;



do
{
ode45_onestep(A,dt,num_of_equations);


for (int i=0; i<5; i++){
	gsl_matrix_set(A,i,0,gsl_matrix_get(A,i,1));
}






t = t + dt;

if(counter%100==0){
double photon= fabs(gsl_matrix_get(A,0,0))-1;
printf("%1f	%1f	%1f\n", t, gsl_matrix_get(A,1,0)/2, photon);}

counter++;

}while(t<time_end);

return(0);
}
