#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

double EQ0(double ASp_imag, double AAd_real){
		double g= 0.5;
		double kappa= 1.0;
		int N= 10^5; 
		double result= -2*g*N*ASp_imag - kappa*(AAd_real-1);
		return(result);}


double EQ1(double Sz_real, double ASp_imag){
		double g= 0.5;
		double result= 4*g*ASp_imag;
		return(result);}

double EQ2(double SpSm_imag, double ASp_real){
		double g= 0.5;
		double kappa= 1.0;
		int N= 10^5; 
		double result= g*(N-1)*SpSm_imag - (kappa/2)*ASp_real;
		return(result);}

double EQ3(double Sz_real, double AAd_real, double ASp_imag, double SpSm_real){
		double g= 0.5;
		double kappa= 1.0;
		int N= 10^5; 
		double result=-(g/2)*(1-Sz_real) - g*Sz_real*AAd_real - (kappa/2)*ASp_imag - g*(N-1)*SpSm_real;
		return(result);} 

double EQ4(double Sz_real, double ASp_imag, double SpSm_imag){
		double g= 0.5;
		double result= -1*g*Sz_real*ASp_imag;
		return(result);	}

double EQ5(double Sz_real, double ASp_real, double SpSm_real){
		double g= 0.5;
		double result= -1*g*Sz_real*ASp_real;
		return(result);	}




double ode45_onestep(gsl_matrix* A, double dt, int num_of_equations)
{

		double AAd_real= 	gsl_matrix_get(A,0,0);
		double Sz_real= 	gsl_matrix_get(A,1,0);
		double ASp_real = 	gsl_matrix_get(A,2,0);
		double ASp_imag= 	gsl_matrix_get(A,3,0);
		double SpSm_real= 	gsl_matrix_get(A,4,0);
		double SpSm_imag= 	gsl_matrix_get(A,5,0);


		double k1, k2, k3, k4, result;

		// Update for <aa+>
		k1= dt * EQ0(ASp_imag, AAd_real);
		k2= dt * EQ0(ASp_imag+(k1/2), AAd_real+(k1/2));
		k3= dt * EQ0(ASp_imag+(k2/2), AAd_real+(k2/2));
		k4= dt * EQ0(ASp_imag+k3, AAd_real+k3);
		result= gsl_matrix_get(A,0,0) + (1.0/6)*(k1 + 2*k2 + 2*k3 + k4);
		gsl_matrix_set(A,0,1,result);

		
		// Update for <Sz>
 		k1= dt * EQ1(Sz_real, ASp_imag);
		k2= dt * EQ1(Sz_real+(k1/2), ASp_imag+(k1/2));
		k3= dt * EQ1(Sz_real+(k2/2), ASp_imag+(k2/2));
		k4= dt * EQ1(Sz_real+k3, ASp_imag+k3);
		result= gsl_matrix_get(A,1,0) + (1.0/6)*(k1 + 2*k2 + 2*k3 + k4);
		gsl_matrix_set(A,1,1,result);


		// Update for Real[<ASp>]
 		k1= dt * EQ2(SpSm_imag, ASp_real);
		k2= dt * EQ2(SpSm_imag+(k1/2), ASp_real+(k1/2));
		k3= dt * EQ2(SpSm_imag+(k2/2), ASp_real+(k2/2));
		k4= dt * EQ2(SpSm_imag+k3, ASp_real+k3);
		result= gsl_matrix_get(A,2,0) + (1.0/6)*(k1 + 2*k2 + 2*k3 + k4);
		gsl_matrix_set(A,2,1,result);
		
		
		// Update for Imag[<ASp>]
 		k1= dt * EQ3(Sz_real,        AAd_real,        ASp_imag,		SpSm_real);
		k2= dt * EQ3(Sz_real+(k1/2), AAd_real+(k1/2), ASp_imag+(k1/2),	SpSm_real + (k1/2));
		k3= dt * EQ3(Sz_real+(k2/2), AAd_real+(k2/2), ASp_imag+(k2/2),	SpSm_real + (k2/2));
		k4= dt * EQ3(Sz_real+k3,     AAd_real+k3,     ASp_imag+k3,	SpSm_real + k3);
		result= gsl_matrix_get(A,3,0) + (1.0/6)*(k1 + 2*k2 + 2*k3 + k4);
		gsl_matrix_set(A,3,1,result);

		
		// Update for Real[<SpSm>]
 		k1= dt * EQ4(Sz_real,        ASp_imag,        SpSm_imag);
		k2= dt * EQ4(Sz_real+(k1/2), ASp_imag+(k1/2), SpSm_imag+(k1/2));
		k3= dt * EQ4(Sz_real+(k2/2), ASp_imag+(k2/2), SpSm_imag+(k2/2));
		k4= dt * EQ4(Sz_real+k3,     ASp_imag+k3,     SpSm_imag+k3);
		result= gsl_matrix_get(A,4,0) + (1.0/6)*(k1 + 2*k2 + 2*k3 + k4);
		gsl_matrix_set(A,4,1,result);

		// Update for Imag[<SpSm>]
 		k1= dt * EQ4(Sz_real,        ASp_real,        SpSm_real);
		k2= dt * EQ4(Sz_real+(k1/2), ASp_real+(k1/2), SpSm_real+(k1/2));
		k3= dt * EQ4(Sz_real+(k2/2), ASp_real+(k2/2), SpSm_real+(k2/2));
		k4= dt * EQ4(Sz_real+k3,     ASp_real+k3,     SpSm_real+k3);
		result= gsl_matrix_get(A,5,0) + (1.0/6)*(k1 + 2*k2 + 2*k3 + k4);
		gsl_matrix_set(A,5,1,result);
}






