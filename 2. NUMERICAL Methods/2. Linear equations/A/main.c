#include <stdio.h>
#include <stdlib.h>
#include "function.h"

int main(void)
{
int N= 4; // Specify number of rows
int M= 3; // Specify number of columns

double** A = matrix_alloc(N,M);
//double **A;
//A = (double **) malloc(SIZE * sizeof(char*));
double** result = matrix_alloc(M,M);
//(double result[M][M];
for (int i=0;i<M;i++){
for (int j=0;j<N;j++){
A[j][i]= rand() %10;
}
}
// Display the matrix
printf("The chosen (%dx%d) matrix is as follows- \n", N,M);
for (int i=0;i<N;i++){
for (int j=0;j<M;j++){
printf("%1f	",A[i][j]);
}
printf("\n");
}


qr_factorization(A,result, N, M);


printf("\n");
printf("\n");
printf("The R matrix of (%dx%d) dimension after QR  factorization is- \n", M,M);
for (int i=0;i<M;i++){
for (int j=0;j<M;j++){
printf("%1f	",result[i][j]);
}
printf("\n");
}

printf("\n");
printf("\n");
printf("THE ABOVE MATRIX IS A UPPER TRIANGULAR MATRIX AS EXPECTED \n");
printf("THE DOT PRODUCTS BETWEEN COLUMNS OF Q GIVING 0 SIGNIFIES THAT THE VECTORS ARE ORTHONORMAL i.e. Transpose(Q)Q=I \n \n \n");


// Solution of linear equations

printf("The B matrix chosen at random is as follows- \n");
double B[M];
double solution[M];
for (int i=0;i<M;i++)
{
B[i]= rand() %10;
solution[i]= 1;
printf("%1f\n", B[i]);
}

printf("\n");
printf("\n");
printf("The solution of the linear equations are- \n");
int j= M-1;
for(int i=M-1;i>=0;i--){
double sum=0.0;
double temp= 1.0;
for(j=M-1;j>i;j--){
sum+= result[i][j]*solution[j];};
temp= B[i]-sum;
solution[i]= temp/(result[i][j]);
}

for(int k=0;k<M;k++)
printf("%1f\n",solution[k]);

printf("\n\nMANUAL CALCULATION of SOLUTUIONS CONFIRM THE RESULTS");









return 0;
}	

