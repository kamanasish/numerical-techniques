/* Numerical Methods- Linear Equations */


#include <stdio.h>
#include <stdlib.h>
#include "function.h"

int main(void)
{
int N= 5; // Specify number of rows
int M= 5; // Specify number of columns

double** A = matrix_alloc(N,M);
//double **A;
//A = (double **) malloc(SIZE * sizeof(char*));
double** result = matrix_alloc(M,M);
//(double result[M][M];
for (int i=0;i<M;i++){
for (int j=0;j<N;j++){
A[j][i]= rand() %100;
}
}
// Display the matrix
printf("The chosen (%dx%d) matrix is as follows- \n", N,M);
for (int i=0;i<N;i++){
for (int j=0;j<M;j++){
printf("%1f	",A[i][j]);
}
printf("\n");
}


qr_factorization(A,result, N, M);


printf("\n");
printf("\n");
printf("The R matrix of (%dx%d) dimension after QR  factorization is- \n", M,M);
for (int i=0;i<M;i++){
for (int j=0;j<M;j++){
printf("%1f	",result[i][j]);
}
printf("\n");
}
printf("\n");
printf("\n");



double B[M];
double matrix[M][M];

for(int loop1=0; loop1<M; loop1++){
double solution[M];
for(int loop2=0;loop2<M;loop2++){
B[loop2]=0;
solution[loop2]= 1;}
B[loop1]=1;


int j= M-1;
for(int i=M-1;i>=0;i--){
double sum=0.0;
double temp= 1.0;
for(j=M-1;j>i;j--){
sum+= result[i][j]*solution[j];};
temp= B[i]-sum;
solution[i]= temp/(result[i][j]);
}

for(int j=0; j<M; j++)
matrix[j][loop1]=solution[j];

}


printf("The inverse of the matrix is- \n");
for (int i=0;i<M;i++){
for (int j=0;j<M;j++){
printf("%1f	",matrix[i][j]);
}
printf("\n");
}

printf("\n");
printf("\n");

//Calculating the unit result
double sum= 0.0;
double multiply[M][M];

    for (int c = 0; c < M; c++) {
      for (int d = 0; d < M; d++) {
        for (int k = 0; k < M; k++) {
          sum = sum + result[c][k]*matrix[k][d];
        }
 
        multiply[c][d] = sum;
        sum = 0;
      }
    }



printf("THE PRODUCT OF THE MATRIX AND ITS INVERSE MUST GIVE A UNIT MATRIX...... CHECKING..... \n");
printf("\n");
printf("\n");

for (int i=0;i<M;i++){
for (int j=0;j<M;j++){
printf("%1f	",multiply[i][j]);
}
printf("\n");
}

printf("\n");
printf("\n");


printf("THE ALGORITHM IS WORKING CORRECTLY...... CONFIRMED \n");





return 0;
}	

