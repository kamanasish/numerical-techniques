#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#define PI 3.14159265


double linterp(int n, double *x, double *y, double z)
{
#include<assert.h>
assert(n>1 && z>x[0] && z<=x[n-1]);
int i=0, j= n-1;
while (j-i>1)
{
int m= (i+j)/2;
if(z>x[m])
i=m;
else
j=m;
}
double slope= (y[i+1]-y[i])/(x[i+1]-x[i]);
double area= (slope)*(z-x[0]);

return(area);
}



int main(void)
{
double x[20];
double y[20];
int n= 20;
float val= PI/180;

for (int l= 0; l< 20; l+=1)
{
x[l]= val*l*18;
y[l]= sin(x[l]);
}



for (int l= 1; l< 19; l+=1)
{
double z= val*l*18;
double result= linterp(n, x, y, z);
double final= result;
printf("%1f %1f \n", z, final);
}
return 0;
}






