#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "function.h"


int main(void)
{
int N= 9; // Specify number of rows
int M= 3; // Specify number of columns

double** A = matrix_alloc(N,M);
double** result = matrix_alloc(M,M);
double** mt = matrix_alloc(M,N);

double x[9][1]={{0.1}, {1.33}, {2.55}, {3.78}, {5}, {6.22}, {7.45}, {8.68}, {9.9}}; // Supplied X values
double y[9][1]={{-15.3}, {0.32}, {2.45}, {2.75}, {2.27}, {1.35}, {0.157}, {-1.23}, {-2.75}}; // Supplied Y values
double z[9]={1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478}; // Errorbar

// Creating the A matrix
for(int i=0;i<N;i++){
A[i][0]=1;
A[i][1]=log(x[i][0]);
A[i][2]=(x[i][0]);
//printf("%1f %1f %1f\n",x[i][0],y[i][0],z[i]);
}


qr_factorization(A,result, mt, N, M);

// Matrix Multiplication of mt and y....... mt has dimension (M x N) and y has dimension (N x 1)
double Bo[M][1];
for(int i=0;i<M;i++){
for(int j=0;j<M;j++){
Bo[i][j]=0.0;}}

for(int i=0;i<M;i++)
for(int j=0;j<1;j++)
for(int k=0;k<N;k++){
Bo[i][j]+=mt[i][k]*y[k][j];
}


// Solution of linear equations


double B[M];
double solution[M];
for (int i=0;i<M;i++)
{
B[i]= Bo[0][i];
solution[i]= 1;
}

int j= M-1;
for(int i=M-1;i>=0;i--){
double sum=0.0;
double temp= 1.0;
for(j=M-1;j>i;j--){
sum+= result[i][j]*solution[j];};
temp= B[i]-sum;
solution[i]= temp/(result[i][j]);
}

for (float i=0.1; i<10; i+=0.2){
double y_value= solution[0]*1 + solution[1]*log(i) + solution[2]*i;
//printf("%1f 	%1f\n", i,y_value);
}









// Function for the inverse of the matrix

double trans[M][N];
double AA[M][M];
// A is a (N x M) dimension matrix
// A transpose is (M x N) dimensional matrix
// At*A is (M x M) dimensional block

for (int i=0; i<N; i++)
for (int j=0; j<M; j++)
trans[j][i]= A[i][j];


// Now the matrix multiplication of trans and A to be stored in AA.

for(int i=0;i<M;i++){
for(int j=0;j<M;j++){
AA[i][j]=0.0;}}

for(int i=0;i<M;i++)
for(int j=0;j<M;j++)
for(int k=0;k<N;k++){
AA[i][j]+=trans[i][k]*A[k][j];
}


// Now the final result is the inverse of the AA matrix

int count= 0;
double error[3];
double determinant= 0.0;
for(int i=0;i<M;i++)
      determinant = determinant + (AA[0][i]*(AA[1][(i+1)%M]*AA[2][(i+2)%M] - AA[1][(i+2)%M]*AA[2][(i+1)%M]));
 
   printf("\nThe covariance matrix for TYPE 2 fit is : \n\n");
   for(int i=0;i<M;i++){
      for(int j=0;j<M;j++){
	printf("%.2f\t",((AA[(i+1)%M][(j+1)%M] * AA[(i+2)%M][(j+2)%M]) - (AA[(i+1)%M][(j+2)%M]*AA[(i+2)%M][(j+1)%M]))/ determinant);
	if(i==j)
		{
		error[count]= sqrt(((AA[(i+1)%M][(j+1)%M] * AA[(i+2)%M][(j+2)%M]) - (AA[(i+1)%M][(j+2)%M]*AA[(i+2)%M][(j+1)%M]))/ determinant);
		count++;
		}
}       
printf("\n");
}
 
printf("\n");
printf("\n");
printf("The estimates of the error for TYPE 2 fit are as follows-\n");
for (int i=0;i<3;i++)
printf("%1f\n",error[i]);






return 0;
}	

