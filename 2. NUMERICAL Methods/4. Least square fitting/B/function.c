#include"function.h"
#include<math.h>


double** matrix_alloc(int N, int M)
{
double **array;
array = (double **) malloc(N * sizeof(double*));
for (int row = 0; row<N; row++) {
  array[row] = (double *) malloc(M * sizeof(double));
  }
return array;
}







void qr_factorization(double** A, double** multiply, double** mt, int N, int M)
{
double m[N][M]; // to store orthogonal matrix
double mo[N][M]; // to store normalized orthogonal matrix
double norm;

for (int i=0;i<M;i++){
for (int j=0;j<N;j++){
m[j][i]= A[j][i];
}
}

for (int j=1; j<M; j++){
for (int k=0; k<j; k++){
double dot_product= 0.0;
for (int i=0; i<N; i++){
dot_product+= m[i][j] * m[i][k];
norm+=m[i][k]*m[i][k];
}
double res= 0.0;
for(int i=0; i<N; i++)
{
res= m[i][j]-(dot_product/norm)*m[i][k];
m[i][j]= res;
}
norm= 0.0;
}
}

double norm2[M];
double temp= 0.0;
for (int j=0; j<M; j++){
for (int i=0; i<N; i++){
temp+=m[i][j]*m[i][j];
norm2[j]= temp;
}
temp= .0;
}

// Display the matrix
//printf("\n\n");
//printf("The normalized Gram Schmidt orthonormal (%dx%d) matrix is as follows- \n", N,M);
for (int i=0;i<N;i++){
for (int j=0;j<M;j++){
//printf("%1f	",(1/norm2[j])*m[i][j]);
mo[i][j]= (1/norm2[j])*m[i][j];
}
//printf("\n");
}
// Cross check and display the dot product of the columns
//printf("\n\nORTHONORMALIZATION CROSS CHECK in progress....\n");
for (int j=1;j<M;j++){
double dot_pro= 0.0;
for (int i=0;i<N;i++){
dot_pro+= m[i][0]*m[i][j];}
//printf("The dot product of column 1 and column %d is %1f \n",j+1,dot_pro);
}

// Transpose of matrix

for (int i=0;i<M;i++){
for (int j=0;j<N;j++){
mt[i][j]=mo[j][i];
}}


// Matrix Multiplication of matrices qt and A. qt has (M x N) dimension and A has (N x M) dimensions
// Code for matrix multiplication

for(int i=0;i<M;i++){
for(int j=0;j<M;j++){
multiply[i][j]=0.0;}}

for(int i=0;i<M;i++)
for(int j=0;j<M;j++)
for(int k=0;k<N;k++){
multiply[i][j]+=mt[i][k]*A[k][j];
}


return;
}
