#include <stdio.h>
#include <stdlib.h>

double func(double x);

double** matrix_alloc(int N, int M);

double rkstep4(double dt, double t);

double rkstep4_two_dt(double dt1, double t);

void rkstep4_one_dt(double dt, double t, double y, double** yy);

double rkstep4_half_dt(double dt, double t, double y);



