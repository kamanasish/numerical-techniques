#include"function.h"
#include<math.h>

double** matrix_alloc(int N, int M)
{
double **array;
array = (double **) malloc(N * sizeof(double*));
for (int row = 0; row<N; row++) {
  array[row] = (double *) malloc(M * sizeof(double));
  }
return array;
}




double func(double x)
{
double temp= cos(x);
return (temp);
}


// Runge Kutta method
double rkstep4(double dt, double t)
{
double k1= dt* (func(t));
double k2= dt* (func(t+(dt/2)));
double k3= dt* (func(t+(dt/2)));
double k4= dt* (func(t+dt));

double result= ((k1 + (2*k2) + (2*k3) + k4)/6);
return (result);
}





double rkstep4_two_dt(double dt1, double t)
{
double dt= 2*dt1;
double k1= dt* (func(t));
double k2= dt* (func(t+(dt/2)));
double k3= dt* (func(t+(dt/2)));
double k4= dt* (func(t+dt));

double result= ((k1 + (2*k2) + (2*k3) + k4)/6);
return (result);
}





void rkstep4_one_dt(double dt, double t, double y, double** yy)
{
int count= 0;
double result= y;
for (int i=0;i<2;i++){
double k1= dt* (func(t));
double k2= dt* (func(t+(dt/2)));
double k3= dt* (func(t+(dt/2)));
double k4= dt* (func(t+dt));

result= result + ((k1 + (2*k2) + (2*k3) + k4)/6);
yy[i][0]= result;
} 
return;
}



double rkstep4_half_dt(double dt, double t, double y)
{
dt=dt/2;
int count= 0;
double result= y;
for(int i=0;i<2;i++){
double k1= dt* (func(t));
double k2= dt* (func(t+(dt/2)));
double k3= dt* (func(t+(dt/2)));
double k4= dt* (func(t+dt));
result= result + ((k1 + (2*k2) + (2*k3) + k4)/6);
} 
return (result);
}


