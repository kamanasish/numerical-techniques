/* Using Runge Kutta 4th order routine for calculating the the differential equation of sin(x) */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "function.h"



int main(void)
{
double** yy = matrix_alloc(2,1);


double precision= 0.001;
double dt= 0.0001;
double t= 0.0;
double t_end= 10*3.14;
double y= 0.0;
int count= 1;                // Adaptive counter check
double adaptive= 0.0;
double increase= 1.0;
int npoints= 200;            // Number of stored points
double y_ax[npoints];
double x_ax[npoints];

printf("X					f(X)							dt\n\n");
do{
count++;
int check= 1.0;


rkstep4_one_dt(dt, t, y, yy);



double y_half= rkstep4_half_dt(dt,t, y);
if ((yy[0][0]-y_half)>precision){
dt= dt/2;
printf("\n\n..............ADAPTIVE ALGORITHM USED.....STEP SIZE dt decreased.................\n\n");
check= 2.0;
}
else
dt=dt;


if(check!=2){
double two_dt= y + rkstep4_two_dt(dt,t);
rkstep4_one_dt(dt, t, y, yy);
if ((yy[1][0]-two_dt)<precision){
dt= 2*dt;
printf("\n\n..............ADAPTIVE ALGORITHM USED.....STEP SIZE dt increased.................\n\n");
}
else
dt=dt;
}










double step1= rkstep4(dt,t); // normal step
double y2= y+step1;
t=t+dt;
y=y2;

printf("%1f				%1f					%1f\n", t, y,dt);
}while(t<t_end);

return 0;
}


















