#include"function.h"
#include<math.h>



double func(double x)
{
double temp= cos(x);
return (temp);
}


// Runge Kutta method
double rkstep4(double dt, double t)
{
double k1= dt* (func(t));
double k2= dt* (func(t+(dt/2)));
double k3= dt* (func(t+(dt/2)));
double k4= dt* (func(t+dt));

double result= ((k1 + (2*k2) + (2*k3) + k4)/6);
return (result);
}
