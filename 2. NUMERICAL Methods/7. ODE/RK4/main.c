/* Using Runge Kutta 4th order routine for calculating the the differential equation of sin(x) */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "function.h"



int main(void)
{

double dt= 0.0001;
double t= 0.0;
double t_end= 4*3.14;
double y= 0.0;
printf("X			f(X)				ERROR				EXACT Value \n\n");



do{
double step1= rkstep4(dt,t);
double step2= rkstep4(dt/2,t);
y = y + step1;
double error= step1-step2;
t=t+dt;
printf("%1f		%1f			%1f			%1f\n", t, y, error, sin(t));
}while(t<t_end);

return 0;
}






