#include"function.h"
#include<math.h>

// Matrix Allocation
double** matrix_alloc(int N, int M)
{
double **array;
array = (double **) malloc(N * sizeof(double*));
for (int row = 0; row<N; row++) {
array[row] = (double *) malloc(M * sizeof(double));
}
return array;
}



// This function returns the delta_f one dimensional matrix
void return_del_f(double** x, double** del_f)
{
double x0= x[0][0];
double y0= x[1][0];
del_f[0][0]= 2*(x0-1) - 400*x0*(y0-(x0*x0));
del_f[1][0]= 200*(y0-(x0*x0));
return;
}


// This function returns the Hessian matrix
void return_Hessian(double** x, double** Hessian)
{
double x0= x[0][0];
double y0= x[1][0];
Hessian[0][0]= 2-(400*y0)+1200*(x0*x0);
Hessian[0][1]= -400*x0;

Hessian[1][0]= -400*x0;
Hessian[1][1]= 200;
return;
}



// This function solves the linear equation using QR decomposition
void qr_factorization(double** del_f, double** Hessian, double** roots)
{

int N= 2;
int M=N;
double m[N][M]; // to store orthogonal matrix
double mo[N][M]; // to store normalized orthogonal matrix
double mt[M][N];
double norm;

for (int i=0;i<M;i++){
for (int j=0;j<N;j++){
m[j][i]= Hessian[j][i];
}
}


//printf("The A matrix is as follows:- \n");
//printf("%1f, %1f\n", A[0][0], A[0][1]);
//printf("%1f, %1f\n\n", A[1][0], A[1][1]);


for (int j=1; j<M; j++){
for (int k=0; k<j; k++){
double dot_product= 0.0;
for (int i=0; i<N; i++){
dot_product+= m[i][j] * m[i][k];
norm+=m[i][k]*m[i][k];
}
double res= 0.0;
for(int i=0; i<N; i++)
{
res= m[i][j]-(dot_product/norm)*m[i][k];
m[i][j]= res;
}
norm= 0.0;
}
}

double norm2[M];
double temp= 0.0;
for (int j=0; j<M; j++){
for (int i=0; i<N; i++){
temp+=m[i][j]*m[i][j];
norm2[j]= temp;
}
temp= .0;
}

// Orthonormal matrix
for (int i=0;i<N;i++){
for (int j=0;j<M;j++){
mo[i][j]= (1/sqrt(norm2[j]))*m[i][j];
}
}


//printf("The Q matrix is as follows:- \n");
//printf("%1f, %1f\n", mo[0][0], mo[0][1]);
//printf("%1f, %1f\n\n", mo[1][0], mo[1][1]);

// Transpose of matrix

for (int i=0;i<M;i++){
for (int j=0;j<N;j++){
mt[i][j]=mo[j][i];
}}


// Matrix Multiplication of matrices qt and A. qt has (M x N) dimension and A has (N x M) dimensions
// Code for matrix multiplication
double multiply[N][M];

for(int i=0;i<M;i++){
for(int j=0;j<M;j++){
multiply[i][j]=0.0;}}

for(int i=0;i<M;i++)
for(int j=0;j<M;j++)
for(int k=0;k<N;k++){
multiply[i][j]+=mt[i][k]*Hessian[k][j];
}
//printf("The R matrix is as follows:- \n");
//printf("%1f, %1f\n", multiply[0][0], multiply[0][1]);
//printf("%1f, %1f\n\n", multiply[1][0], multiply[1][1]);


// Modified eqn which is the multiplication of mt(N x N) and eqn (Nx1)
double rhs[N][1];
double sum2=0;
for (int c = 0; c < N; c++) {
      for (int d = 0; d < 1; d++) {
        for (int k = 0; k < N; k++) {
          sum2 = sum2 - mt[c][k]*del_f[k][d];
        }
 
        rhs[c][d] = sum2;
        sum2 = 0;
      }
    }

// Back substitution
for (int i=0;i<M;i++)
{
roots[i][0]= 1;
}


int j= M-1;
for(int i=M-1;i>=0;i--){
double sum=0.0;
double temp= 1.0;
for(j=M-1;j>i;j--){
sum+= multiply[i][j]*roots[j][0];}
temp= rhs[i][0]-sum;
roots[i][0]= temp/(multiply[i][j]);
}


return;
}



double fxy(double** x)
{
double x0=x[0][0];
double y0=x[1][0];
double res= (1-x0)*(1-x0) + 100*(y0-(x0*x0))*(y0-(x0*x0));
return (res);
}
































