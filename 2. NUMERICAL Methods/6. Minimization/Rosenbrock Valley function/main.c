#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "function.h"


int main(void)
{
int N= 2; 
double** del_f= matrix_alloc(N,1);
double** Hessian= matrix_alloc(N,N);
double** result= matrix_alloc(N,1);
double** x= matrix_alloc(N,1);


x[0][0]= 10;			// Best guess x value
x[1][0]= 25; 			// Best guess y value

int count= 1;

do{
count++;
return_del_f(x, del_f); 
return_Hessian(x, Hessian);
qr_factorization(del_f, Hessian, result); 

x[0][0]= x[0][0] + result[0][0];
x[1][0]= x[1][0] + result[1][0];

}while(fxy(x)>0.0001);


printf("THE ROOTS OF THE EQUATION using MINIMIZATION ARE AS FOLLOWS");
printf("\n x= %1f", x[0][0]);
printf("\n y= %1f", x[1][0]);
printf("\n\nNumber of steps taken: %d", count);
double result2= fxy(x);
printf("\n\nPlugging the solutions in main equation yields %1f", result2);
if (result2<0.0001)
printf("\n\nALGORITHM IS CORRECT AND WITHIN ACCURACY");

else
printf("ERROR !!! DEBUG");
return 0;
}

