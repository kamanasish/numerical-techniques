#include <stdio.h>
#include <stdlib.h>

double** matrix_alloc(int N, int M);

void return_del_f(double** x, double** del_f);

void return_Hessian(double** x, double** Hessian);

void qr_factorization(double** del_f, double** Hessian, double** result);

double fxy(double** x);



