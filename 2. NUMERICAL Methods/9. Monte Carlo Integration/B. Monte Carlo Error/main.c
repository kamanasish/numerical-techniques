/* Checking how the error scales in normal one dimensional monte carlo */

#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include"function.h"



int main(void)
{

double a= 0.0;		// Starting point of integral
double b= 3.14;		// End point of integral
int dim= 1;		// Dimensions considered
double x[dim];
double result[dim];
double sum= 0.0;
double sum2= 0.0;


for (int N= 1; N<400; N+=1)
{

double volume= 1.0;
for(int i=0; i<dim; i++){
volume= volume*(b-a);}
sum= 0.0;
sum2= 0.0;

for(int i=0; i<N; i++)
{
random_generator(dim, a, b, x);
func(x, dim, result);
double fx= result[0];
sum= sum + fx;
sum2= sum2 + (fx*fx);
}


double average= sum/N;
double variance= sum2/N - (average*average);
double error= sqrt(variance/N)*volume;
double final_result= average*volume;

printf("%d	%1f\n", N, error);
}
	
return 0;
}
