/* Checking how the error scales in normal one dimensional monte carlo */

#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include"function.h"



int main(void)
{
int dim= 3;
double a[dim];
double b[dim];

a[0]= 0.0;		// Starting point of first integral
b[0]= 3.14;		// End point of first integral

a[1]= 0.0;		// Starting point of second integral
b[1]= 3.14;		// End point of second integral

a[2]= 0.0;		// Starting point of third integral
b[2]= 3.14;		// End point of third integral

		
double x[dim];
double sum= 0.0;
double sum2= 0.0;
double volume;


for (int N=1; N<5000; N+=1)
{

volume= 1.0;
for(int i=0; i<dim; i++){
volume= volume*(b[i]-a[i]);}

sum= 0.0;
sum2= 0.0;

for(int i=0; i<N; i++)
{
random_generator(dim, a, b, x);
double fx= func(x, dim);
sum= sum + fx;
sum2= sum2 + (fx*fx);
}


double average= sum/N;
double variance= sum2/N - (average*average);
double error= sqrt(variance/N)*volume;
double final_result= average*volume;

printf("%d	%1f\n", N, (1/(3.14*3.14*3.14))*final_result);
}
	
return 0;
}
