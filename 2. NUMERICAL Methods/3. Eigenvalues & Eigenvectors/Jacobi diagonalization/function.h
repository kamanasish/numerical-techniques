#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>


int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int size);
