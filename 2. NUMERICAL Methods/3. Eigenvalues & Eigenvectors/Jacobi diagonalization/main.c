#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"function.h"


int main(void)
{

int size = 4;    // Size of the symmetric matrix for calculating eigenvalues


gsl_matrix * A = gsl_matrix_calloc (size, size);
gsl_matrix * V = gsl_matrix_alloc (size, size);
gsl_vector * e= gsl_vector_alloc(size);



// Generating a symmetric matrix
for(int i=0; i<size; i++){
for(int j=0; j<=i; j++){
gsl_matrix_set(A,i,j,rand() %100);
gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
}
}




printf("The selected symmetric matrix is- \n");
for(int i=0; i<size; i++){
for(int j=0; j<size; j++){
printf("%1f		", gsl_matrix_get(A,i,j)); 
}
printf("\n");
}

int sweeps= jacobi(A, e, V, size);





// PRINTING THE EIGENVALUES
printf("\n\nThe Eigenvalues of the above matrix are-\n");
for(int i=0;i<size;i++){
printf("%d.	%1f\n", i+1, gsl_vector_get(e,i));
}
printf("\n\nThe number of sweeps takes was %d", sweeps);





// PRINTING THE EIGENVECTORS
printf("\n\nThe Eigenvectors of the matrix is- \n");
for(int i=0; i<size; i++){
for(int j=0; j<size; j++){
printf("%1f		", gsl_matrix_get(V,i,j)); 
}
printf("\n");
}

gsl_matrix * E = gsl_matrix_calloc (size, size);
for(int i=0; i<size; i++){
for(int j=0; j<size; j++){
if (i==j)
gsl_matrix_set(E,i,j,gsl_vector_get(e,i));
}
}

printf("\n\n\n**************************************************************************************** \n\n\n");

// CROSS CHECKING THE RESULTS
gsl_matrix * C = gsl_matrix_calloc (size, size);
gsl_matrix * D = gsl_matrix_calloc (size, size);
gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, V, E, 0.0, C);
gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, C, V, 0.0, D);
printf("\nV*D*Trans(V) should return the original matrix A if the algorithm is correct- \n\n");
printf("\nCROSS CHECKING........ The resultant matrix is- \n\n");
for(int i=0; i<size; i++){
for(int j=0; j<size; j++){
printf("%1f		", gsl_matrix_get(D,i,j)); 
}
printf("\n");
} 

printf("\n\nThis is same as the original matrix A......Hence the algorithm is working correctly");
//clock_t end = clock();
//double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
//printf("%d	%1f	%d \n", size, time_spent, size*size*size);



return 0;
}


