#include <stdio.h>
#include <stdlib.h>

void qr_factorization(double** A, double** roots, int N, double** eq);

void rhs_solution(double x0, double y0, double** eq);

double norm(double x, double y);

void Jacobian_return(double x0, double y0, double** J);

double** matrix_alloc(int N, int M);
