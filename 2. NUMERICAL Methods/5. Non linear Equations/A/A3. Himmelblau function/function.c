#include"function.h"
#include<math.h>

// Matrix Allocation
double** matrix_alloc(int N, int M)
{
double **array;
array = (double **) malloc(N * sizeof(double*));
for (int row = 0; row<N; row++) {
  array[row] = (double *) malloc(M * sizeof(double));
  }
return array;
}


// RHS of the equation
void rhs_solution(double x0, double y0, double** eq)
{
double temp1= (x0*x0)+y0-11;
double temp2= x0+(y0*y0)-7;

eq[0][0]= -(4*x0*temp1 + 2*temp2);				
eq[1][0]= -(2*temp1 + 4*y0*temp2);
return;	
}


// Jacobian of the matrix
void Jacobian_return(double x0, double y0, double dx, double dy, double** J)
{
double X= x0+dx;
double Y= y0+dy;

J[0][0]= ((4*X*((X*X)+y0-11) + 2*(X+(y0*y0)-7)) - (4*x0*((x0*x0)+y0-11) + 2*(x0+(y0*y0)-7)))/dx;
J[0][1]= ((4*x0*((x0*x0)+Y-11) + 2*(x0+(Y*Y)-7)) - (4*x0*((x0*x0)+y0-11) + 2*(x0+(y0*y0)-7)))/dy;

J[1][0]= ((2*((X*X)+y0-11) + 4*y0*(X+(y0*y0)-7)) - 2*((x0*x0)+y0-11) + 4*y0*(x0+(y0*y0)-7))/dx;
J[1][1]= ((2*((x0*x0)+Y-11) + 4*Y*(x0+(Y*Y)-7)) - 2*((x0*x0)+y0-11) + 4*y0*(x0+(y0*y0)-7))/dy;

return;
}



// QR factorization and solution
void qr_factorization(double** A, double** roots, int N, double** eqn)
{
int M=N;
double m[N][M]; // to store orthogonal matrix
double mo[N][M]; // to store normalized orthogonal matrix
double mt[M][N];
double norm;

for (int i=0;i<M;i++){
for (int j=0;j<N;j++){
m[j][i]= A[j][i];
}
}


//printf("The A matrix is as follows:- \n");
//printf("%1f, %1f\n", A[0][0], A[0][1]);
//printf("%1f, %1f\n\n", A[1][0], A[1][1]);


for (int j=1; j<M; j++){
for (int k=0; k<j; k++){
double dot_product= 0.0;
for (int i=0; i<N; i++){
dot_product+= m[i][j] * m[i][k];
norm+=m[i][k]*m[i][k];
}
double res= 0.0;
for(int i=0; i<N; i++)
{
res= m[i][j]-(dot_product/norm)*m[i][k];
m[i][j]= res;
}
norm= 0.0;
}
}

double norm2[M];
double temp= 0.0;
for (int j=0; j<M; j++){
for (int i=0; i<N; i++){
temp+=m[i][j]*m[i][j];
norm2[j]= temp;
}
temp= .0;
}

// Orthonormal matrix
for (int i=0;i<N;i++){
for (int j=0;j<M;j++){
mo[i][j]= (1/sqrt(norm2[j]))*m[i][j];
}
}


//printf("The Q matrix is as follows:- \n");
//printf("%1f, %1f\n", mo[0][0], mo[0][1]);
//printf("%1f, %1f\n\n", mo[1][0], mo[1][1]);

// Transpose of matrix

for (int i=0;i<M;i++){
for (int j=0;j<N;j++){
mt[i][j]=mo[j][i];
}}


// Matrix Multiplication of matrices qt and A. qt has (M x N) dimension and A has (N x M) dimensions
// Code for matrix multiplication
double multiply[N][M];

for(int i=0;i<M;i++){
for(int j=0;j<M;j++){
multiply[i][j]=0.0;}}

for(int i=0;i<M;i++)
for(int j=0;j<M;j++)
for(int k=0;k<N;k++){
multiply[i][j]+=mt[i][k]*A[k][j];
}
//printf("The R matrix is as follows:- \n");
//printf("%1f, %1f\n", multiply[0][0], multiply[0][1]);
//printf("%1f, %1f\n\n", multiply[1][0], multiply[1][1]);


// Modified eqn which is the multiplication of mt(N x N) and eqn (Nx1)
double rhs[N][1];
double sum2=0;
for (int c = 0; c < N; c++) {
      for (int d = 0; d < 1; d++) {
        for (int k = 0; k < N; k++) {
          sum2 = sum2 + mt[c][k]*eqn[k][d];
        }
 
        rhs[c][d] = sum2;
        sum2 = 0;
      }
    }

// Back substitution
for (int i=0;i<M;i++)
{
roots[i][0]= 1;
}


int j= M-1;
for(int i=M-1;i>=0;i--){
double sum=0.0;
double temp= 1.0;
for(j=M-1;j>i;j--){
sum+= multiply[i][j]*roots[j][0];}
temp= rhs[i][0]-sum;
roots[i][0]= temp/(multiply[i][j]);
}

return;
}



double norm(double x, double y)
{
double temp1= (x*x)+y-11;
double temp2= x+(y*y)-7;
double fx1= (4*x*temp1 + 2*temp2)*(4*x*temp1 + 2*temp2);
double fx2= (2*temp1 + 4*y*temp2)*(2*temp1 + 4*y*temp2);
double fx= sqrt(fx1+fx2);
return (fx);
}



