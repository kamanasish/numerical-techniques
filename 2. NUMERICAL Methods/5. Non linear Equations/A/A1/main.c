#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "function.h"


int main(void)
{
int N= 2; // Number of variables  	
double** result = matrix_alloc(N,1);
double** eqn = matrix_alloc(N,1);
double** J = matrix_alloc(N,N);
double x0= 0.005;  		// Best guessed x value
double y0= 0.002;     // Best guessed y value 
double dx= 0.01;
double dy= 0.01;
double check1;
double check2;
double x_temp= x0;
double y_temp= y0;
double count= 0;
double iff=0;

do{
rhs_solution(x0,y0,eqn); // This function returns the RHS of the linear equations as an array
Jacobian_return(x0,y0,dx,dy,J); // Returns the Jacobian for supplied values of x,y,dx,dy
qr_factorization(J, result, N, eqn);  // Returns the result of the matrix equation using QR decomposition
double x_new= result[0][0];
double y_new= result[1][0];

double lambda= 1.0;
check1= norm(x0+x_new, y0+y_new);
check2= norm(x0, y0);

do{
count++;
iff=1;
lambda= lambda/2.0;
x_temp= x_temp + lambda*x_new;
y_temp= y_temp + lambda*y_new;
check1= norm(x_temp+lambda*x_new, y_temp+lambda*y_new);
check2= norm(x0, y0);
}while(check1>(1-lambda/2)*check2 && lambda>(1/64));

if (iff!=1){
x0=x_new;
y0=y_new;}

x0=x_temp;
y0=y_temp;

}while(norm(x0,y0)>0.000001);

//norm(x0,y0)>0.1
printf("The roots of the equations are- \n");
printf("x= %1f\n",x0);
printf("y= %1f\n\n",y0);



printf("Plugging the above values in the first equation gives %1f\n", exp(-x0) + exp(-y0)-1-(1/10000));
printf("Plugging the above values in the second equation gives %1f\n", 10000*x0*y0 - 1);
printf("\n \nFOR EXACT VALUE OF x AND y, THE ABOVE RESULTS SHOULD CONVERGE TO ZERO\n");
printf("Hence the algorithm works\n\n");
printf("%1f", count);

return 0;
}	

