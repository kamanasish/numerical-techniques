/* 

THIS CODE CALCULATES THE SINGULAR GAMMA FUNCTION INTRODUCED IN THE MONTE CARLO EXERCISE 

MULTITHREADING IS USED.... IN THIS CASE, 4 THREADS... EACH HANDLING N= 1e8 SAMPLES

ACCURACY INCREASES WITH INCREASE IN THE NUMBER OF SAMPLES. 

*/



#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<omp.h>
#include"function.h"



int main(void)
{
int dim= 3;
double a[dim];
double b[dim];

a[0]= 0.0;		// Starting point of first integral
b[0]= 3.14;		// End point of first integral
a[1]= 0.0;		// Starting point of second integral
b[1]= 3.14;		// End point of second integral
a[2]= 0.0;		// Starting point of third integral
b[2]= 3.14;		// End point of third integral

double x[dim];
double sum[4]= {0.0 ,0.0, 0.0, 0.0};
//double sum2[4]={0.0 ,0.0, 0.0, 0.0};
double volume= 1.0;

int N= 1e7;


for(int i=0; i<dim; i++){
volume= volume*(b[i]-a[i]);}



	#pragma omp parallel sections
	{

		#pragma omp section
		{
			for(int i=0; i<N; i++)
			{
			random_generator(dim, a, b, x);
			double fx1= func(x, dim);
			sum[0]= sum[0] + fx1;
			//sum2[0]= sum2[0] + (fx1*fx1);
			}
		}


		#pragma omp section
		{
			for(int i=0; i<N; i++)
			{
			random_generator(dim, a, b, x);
			double fx2= func(x, dim);
			sum[1]= sum[1] + fx2;
			//sum2[1]= sum2[1] + (fx2*fx2);
			}
		}



		#pragma omp section
		{
			for(int i=0; i<N; i++)
			{
			random_generator(dim, a, b, x);
			double fx3= func(x, dim);
			sum[2]= sum[2] + fx3;
			//sum2[2]= sum2[2] + (fx3*fx3);
			}
		}



		#pragma omp section
		{
			for(int i=0; i<N; i++)
			{
			random_generator(dim, a, b, x);
			double fx4= func(x, dim);
			sum[3]= sum[3] + fx4;
			//sum2[3]= sum2[3] + (fx4*fx4);
			}
		}
	}


double average= (sum[0] + sum[1] + sum[2] + sum[3])/(N*4);
//double variance= ((sum2[0] + sum2[1] + sum2[2] + sum[3])/N_total) - (average*average);
double final_result= average*volume;


double display= (1/(3.14*3.14*3.14))*final_result;
double analytical= 1.3932039296;
double error= ((fabs(display-analytical))/analytical)*100;

printf("The solution of the Singular Gamma function from the Monte Carlo exercise is: %1f\n\n", display);
printf("The analytical original solution is: %1f \n\n", analytical );
printf("Error percentage= %1f %%\n\n", error);

printf("\n\n\n\n*************************** MULTI THREADING DETAILS *******************************************\n\n");

printf("Number of threads used: 4 \n \n \n");
printf("Each thread is sampling %d random samples \n \n \n", N);
printf("Total sampling points= %d\n \n \n", (N*4));

	
return 0;
}
