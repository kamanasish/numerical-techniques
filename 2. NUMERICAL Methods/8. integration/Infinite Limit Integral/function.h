#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>


double adapt(double f(double), double a, double b, double acc, double eps);
double adapt24(double f(double), double a, double b, double acc, double eps, double f2, double f3, int nrec);
double f(double x);
