/* THIS IS A GENERAL PROGRAM TO CALCULATE
NUMERIC INTEGRATION INCLUDING THE ONES WITH LIMIT FROM -InF TO +InF

IN THIS EXAMPLE, WE HAVE CALCULATED THE NUMBERIC INTEGRATION OF 1/(X*X)
FROM x1= 1.0 TO x2= InF.

The solution to this integral is already known and is equal to 1.0

*/ 



#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include<assert.h>
double adapt(double f(double), double a, double b, double acc, double eps);
double adapt24(double f(double), double a, double b, double acc, double eps, double f2, double f3, int nrec);
double f(double x);
double fx(double x);


// Define the function to be evaluated here
double fx(double x)
{
double res= 1/(x*x); // Define here
return(res);
}




// Function adapt24
double adapt24(double f(double), double a, double b, double acc, double eps, double f2, double f3, int nrec)
{
assert(nrec<1000000);
double f1= f(a+(b-a)/6);
double f4= f(a+5*(b-a)/6);
double Q= (2*f1 + f2 + f3 + 2*f4)/6*(b-a);
double q= (f1 + f2 + f3 + f4)/4*(b-a);
double tolerance= acc+eps*fabs(Q);
double error= fabs(Q-q);

if (error<tolerance)
return Q;

else
{
double Q1= adapt24(f, a, (a+b)/2, acc/sqrt(2.0), eps, f1, f2,nrec+1);
double Q2= adapt24(f, (a+b)/2, b,  acc/sqrt(2.0), eps, f3, f4, nrec+1);
return(Q1+Q2);
}
}


// Function adapt
double adapt(double f(double), double a, double b, double acc, double eps)
{
double f2= f(a+2*(b-a)/6);
double f3= f(a+4*(b-a)/6);
int nrec= 0;
return adapt24(f,a,b,acc,eps,f2,f3,nrec);
}

int main(void)
{
int calls= 0;
double a1= 1.0, b1= INFINITY, acc= 0.001, eps= 0.001;
double a,b;
	if(a1==-INFINITY && b1==INFINITY)
		a=-1.0, b= 1.0;
	else if(a1!=-INFINITY && b1==INFINITY)
		a= 0.0, b= 1.0;
	else if(a1==-INFINITY && b1!=INFINITY)
		a= 0.0, b= 1.0;
	else if(a1!=-INFINITY && b1!=INFINITY)
		a= a1, b= b1;


double f(double x)
	{
	calls++;

	if(a1==-INFINITY && b1==INFINITY)
		{
		double y= x/(1-x*x);
		double res1= 1/(y*y);
		double res2=  1+(x*x);
		double res3= (1-(x*x))*(1-(x*x));
		return(res1*(res2/res3));
		}
		
	else if(a1!=-INFINITY && b1==INFINITY)
		{
		double y= a1 + x/(1-x);
		double res1= 1/(y*y);
		double res2=  (1-x)*(1-x);
		return(res1*(1/res2));
		}

	else if(a1==-INFINITY && b1!=INFINITY)
		{
		double res1= fx(a1-(1-x)/x);
		double res2=  1/(x*x);
		return(res1*res2);
		}

	else if(a1!=-INFINITY && b1!=INFINITY)
		{
		double res1= fx(x);
		return(res1);
		}

	}
double Q= adapt(f,a,b,acc,eps);
double ana= 1.0;
printf("The function to be evaluated is 1/(x*x)dx from x=1 to x= Infinity\n");
printf("\n\n");
printf("ANALYTICAL RESULT=  %1f\n", ana);
printf("NUMERIC RESULT= %g \n", Q);
double error_percent= (fabs(ana - Q)/(ana))*100;
printf("ERROR PERCENT = %1f\n\n", error_percent);
printf("\n\n\n");
return 0;
}



