#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include<assert.h>
double adapt(double f(double), double a, double b, double acc, double eps);
double adapt24(double f(double), double a, double b, double acc, double eps, double f2, double f3, int nrec);
double f(double x);






// Function adapt24
double adapt24(double f(double), double a, double b, double acc, double eps, double f2, double f3, int nrec)
{
assert(nrec<1000000);
double f1= f(a+(b-a)/6);
double f4= f(a+5*(b-a)/6);
double Q= (2*f1 + f2 + f3 + 2*f4)/6*(b-a);
double q= (f1 + f2 + f3 + f4)/4*(b-a);
double tolerance= acc+eps*fabs(Q);
double error= fabs(Q-q);

if (error<tolerance)
return Q;

else
{
double Q1= adapt24(f, a, (a+b)/2, acc/sqrt(2.0), eps, f1, f2,nrec+1);
double Q2= adapt24(f, (a+b)/2, b,  acc/sqrt(2.0), eps, f3, f4, nrec+1);
return(Q1+Q2);
}
}


// Function adapt
double adapt(double f(double), double a, double b, double acc, double eps)
{
double f2= f(a+2*(b-a)/6);
double f3= f(a+4*(b-a)/6);
int nrec= 0;
return adapt24(f,a,b,acc,eps,f2,f3,nrec);
}



int main(void)
{
int calls= 0;
double a=0.0, b= 1.0, acc= 0.001, eps= 0.001;
double f(double x)
{
calls++;
double res= log(x)/sqrt(x);
return(res);
}
double Q= adapt(f,a,b,acc,eps);
printf("The function to be evaluated is log x/sqrt(x)dx from x=0 to x= 1\n");
printf("ANALYTICAL RESULT=  -4\n");
printf("NUMERIC RESULT= %g \n", Q);
double error_percent= (fabs(-4 - Q)/(-4))*100;
printf("ERROR PERCENT = %1f\n\n", error_percent);
printf("\n\n\n");

return 0;
}

