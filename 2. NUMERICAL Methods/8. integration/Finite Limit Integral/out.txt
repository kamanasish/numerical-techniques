The function to be evaluated is sqrt(x)dx from x=0 to x= 1
ANALYTICAL RESULT=  0.666666667
NUMERIC RESULT= 0.666954 
ERROR PERCENT = 0.043086


The function to be evaluated is 1/sqrt(x)dx from x=0 to x= 1
ANALYTICAL RESULT=  2
NUMERIC RESULT= 1.99996 
ERROR PERCENT = 0.001925


The function to be evaluated is log x/sqrt(x)dx from x=0 to x= 1
ANALYTICAL RESULT=  -4
NUMERIC RESULT= -3.99992 
ERROR PERCENT = -0.002074




