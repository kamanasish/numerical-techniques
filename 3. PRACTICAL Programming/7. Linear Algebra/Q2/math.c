
#include <gsl/gsl_math.h>
#include <stdio.h>
#include <gsl/gsl_linalg.h>

int
main (void)
{
  double a_data[] = { 6.13, -2.90, 5.86,
                      8.08, -6.31, -3.89,
                      -4.36, 1.00, 0.19 };

  double b_data[] = { 6.23, 5.37, 2.29 };

  gsl_matrix_view m 
    = gsl_matrix_view_array (a_data, 3, 3);

  gsl_vector_view b
    = gsl_vector_view_array (b_data, 3);

  gsl_vector *x = gsl_vector_alloc (3);
  
  int s;

  gsl_permutation * p = gsl_permutation_alloc (3);

  gsl_linalg_LU_decomp (&m.matrix, p, &s);

  gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);

  printf ("The results of the linear equations using LU decomposition are as follows \n");
  gsl_vector_fprintf (stdout, x, "%g");

  gsl_permutation_free (p);
  gsl_vector_free (x);
  return 0;
}
