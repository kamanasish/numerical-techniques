
#include <gsl/gsl_math.h>
#include <stdio.h>
#include <gsl/gsl_sf_airy.h>

int
main (void)
{
for (float x=-10.0; x<=2.0; x=x+0.1){
  double y = gsl_sf_airy_Ai(x, GSL_PREC_APPROX);
  double z = gsl_sf_airy_Bi(x, GSL_PREC_APPROX);
  printf("%f %f\n", x, z);
}
  return 0;
}
