#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_math.h>
#include "function.h"



int main(void)
{
int iter= 0;
int status;

const gsl_multimin_fdfminimizer_type *T;
gsl_multimin_fdfminimizer *s;


double par[2]={1, 100};
gsl_vector *x;
gsl_multimin_function_fdf my_func;

my_func.n = 2;
my_func.f = my_f;
my_func.df = my_df;
my_func.fdf = my_fdf;
my_func.params = par;


/* INITIAL CONDITIONS  */
x = gsl_vector_alloc (2);
gsl_vector_set (x, 0, 5.0);
gsl_vector_set (x, 1, 10.0);



T = gsl_multimin_fdfminimizer_conjugate_fr;
s = gsl_multimin_fdfminimizer_alloc (T, 2);

gsl_multimin_fdfminimizer_set (s, &my_func, x, 0.01, 1e-4);
printf("No. of iteration	x				y					f(x)\n\n");
do
    {
      iter++;
      status = gsl_multimin_fdfminimizer_iterate (s);

      if (status)
        break;

      status = gsl_multimin_test_gradient (s->gradient, 1e-3);

      if (status == GSL_SUCCESS)
        printf ("\n\n\nMINIMUM FOUND AT:\n");

      printf ("%d 			%1f 			%1f 			%1f\n", iter, gsl_vector_get (s->x, 0), gsl_vector_get (s->x, 1),s->f);

    }
  while (status == GSL_CONTINUE && iter < 1000);
printf("\n\n\n");
gsl_multimin_fdfminimizer_free (s);
gsl_vector_free (x);

return 0;
}

































