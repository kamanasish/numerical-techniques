#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_math.h>
/* Defining the function whose minimum is to be calculated */

double my_f (const gsl_vector *v, void *params)
{
double x,y;
double *p= (double *)params;
x= gsl_vector_get(v,0);
y= gsl_vector_get(v,1);

double result= p[0]*(1-x)*(1-x) + p[1]*(y-(x*x))*(y-(x*x));
return(result);
}


/* The gradient of the function */
void my_df (const gsl_vector *v, void *params, gsl_vector *df)
{
double x, y;
double *p = (double *)params;
x = gsl_vector_get(v, 0);
y = gsl_vector_get(v, 1);
double dfdx= -2*p[0]*(1-x) - p[1]*2*2*x*(y-(x*x));
double dfdy= p[1]*2*(y-(x*x));
gsl_vector_set(df, 0, dfdx);
gsl_vector_set(df, 1, dfdy);
}


/* Compute both f and df together. */
void my_fdf (const gsl_vector *x, void *params, double *f, gsl_vector *df)
{
  *f = my_f(x, params);
  my_df(x, params, df);
}

