#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>

// The wavefunction that we are varying
double psi(double x, void* params)
{
	double alpha = *(double*) params;
	return exp( - alpha * x * x / 2 );
}

// The integrand for the (unnormalized) expectation value calculation.
double integrandExpectation(double x, void* params)
{
	double alpha = *(double*) params;
	return (-pow(alpha*x,2)/2 + alpha/2 + pow(x,2)/2) * psi(x,params) * psi(x,params);
}

// The integrand for the normalization calculation.
double integrandNorm(double x, void* params)
{
	return psi(x,params)*psi(x,params);
}

int main()
{
	// Initialize integration variables.
	int limit = 100;
	double epsabs = 1e-6, epsrel = 1e-6, resultNorm, resultExpectation, abserrNorm, abserrExpectation;

	// Initialize workspace.
	gsl_integration_workspace* Workspace = gsl_integration_workspace_alloc(limit);

	// Do the integration for a range of alpha values.
	for(double alpha = 0.01; alpha <= 2; alpha += 0.01)
	{
		// GSL function initialization for norm integral.
		gsl_function FNorm;
		FNorm.function = &integrandNorm;
		FNorm.params = &alpha;

		// GSL function initialization for (unnormalized) energy expectation integral.
		gsl_function FExpectation;
		FExpectation.function = &integrandExpectation;
		FExpectation.params = &alpha;


		// Do the integration.
		gsl_integration_qagi(&FNorm, epsabs, epsrel, limit, Workspace, &resultNorm, &abserrNorm);
		gsl_integration_qagi(&FExpectation, epsabs, epsrel, limit, Workspace, &resultExpectation, &abserrExpectation);

		// Calculate the normalized result.
		double E_alpha = resultExpectation / resultNorm;

		// Print the result.
		printf("%lg \t %lg\n",alpha,E_alpha);
	}

	// Clean up the workspace!
	gsl_integration_workspace_free(Workspace);

	return 0;
}

