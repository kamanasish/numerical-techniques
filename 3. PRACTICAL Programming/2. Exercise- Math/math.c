#include <math.h>
#include <stdio.h>
#include<complex.h>

int main(void)
{

double result1= tgamma(5.0);
double result2= j0(0.5);
double result3= sqrt(2);
double real_exp1= cos(1);
double imag_exp1= sin(1);

double real_exp2= cos(3.14);
double imag_exp2= sin(3.14);




printf("The true Gamma function of 5.0 is %f\n", result1);
printf("The Bessel function value of 0.5 is %f\n", result2);
printf("The square root of -2 is %fi\n", result3);
printf("The exponential of i is %f + i%f \n", real_exp1,imag_exp1);
printf("The exponential of pi is %f + i%f\n", real_exp2,imag_exp2);
printf("\n");



printf("Below we check different variable types\n");
printf("Original number is 0.1111111111111111111111111111L\n");
float x1= 0.1111111111111111111111111111L;
printf("Float truncates to %.25g\n", x1);
double x2= 0.1111111111111111111111111111L;
printf("Double truncates to %.25lg\n", x2);
long double x3= 0.1111111111111111111111111111L;
printf("Long Double truncates to %.25Lg\n",x3);



}
