The true Gamma function of 5.0 is 24.000000
The Bessel function value of 0.5 is 0.938470
The square root of -2 is 1.414214i
The exponential of i is 0.540302 + i0.841471 
The exponential of pi is -0.999999 + i0.001593

Below we check different variable types
Original number is 0.1111111111111111111111111111L
Float truncates to 0.1111111119389533996582031
Double truncates to 0.1111111111111111049432054
Long Double truncates to 0.1111111111111111111096053
