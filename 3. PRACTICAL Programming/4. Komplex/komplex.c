#include<stdio.h>
#include"komplex.h"

void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = {a.re + b.re , a.im + b.im };
	return result;
}


int komplex_equal (komplex a, komplex b) {
	double x1= a.re;
	double x2= a.im;
	double x3= b.re;
	double x4= b.im;
	if(x1==x3 && x2==x4)
	return 1;
	else 
	return 0;
}


komplex komplex_mul (komplex a, komplex b) {
	komplex result = {(a.re * b.re)-(a.im * b.im), (a.re * b.im) + (a.im * b.re)};
	return result;
}


	
komplex komplex_sub (komplex a, komplex b) {
	komplex result = {a.re - b.re , a.im - b.im };
	return result;
}
	
komplex komplex_conjugate(komplex a) {
	komplex result = {a.re , -a.im};
	return result;
}

	
komplex komplex_abs(komplex a) {
	komplex result = {sqrt((a.re*a.re) + (a.im*a.im))};
	return result;
}

komplex komplex_exp(komplex a) {
	double argument = {sqrt((a.re*a.re) + (a.im*a.im))};
	double theta= atan((a.im)/(a.re));
	komplex result2= {argument*cos(theta), argument*sin(theta)};
	return result2;
}
