/* Use Runge Kutta Method to solve a differential equation */

#include<stdio.h>
#include<math.h>
#include<gsl/gsl_sf_erf.h>
#include<gsl/gsl_math.h>


int main(void)
{
double a= -3.0;
double b= 3.0;
double dt= 0.01;

printf("Intial value = %1f\n", a);
printf("Final value=   %1f\n\n", b);
printf("x		erf(x)			dx\n");



do{


// GSL Error function
double result_gsl= gsl_sf_erf(a);		// The exact error function
printf("%1f	%1f	%1f\n",a, result_gsl, dt);



a=a+dt;
}while(a<b);

return 0;
}





