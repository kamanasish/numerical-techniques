#include<math.h>
#include<stdio.h>
#include<complex.h>
#include<limits.h>
#include<float.h>

int main(void)
{

long double max=INT_MAX;
long double sum= 0;
int i=1;
while(i+1>i) 
{
sum=sum+ (1.0/i);
i++;
}
printf("My max integer is %i\n",i);
printf("Max integer using INT_MAX is %.25Lg\n",max);

printf("\n");
printf("\n");

long double min=INT_MIN;
int j=1;
while(j-1<j) 
{j--;}
printf("My min integer is %i\n",j);
printf("Max integer using INT_MAX is %.25Lg\n",min);

printf("\n");
printf("\n");

float x1=1; 
while(1+x1!=1){x1/=2;} x1*=2;

float x2= FLT_EPSILON;

printf("Machine epsilon for float date type using do while is %f\n",x1);
printf("Machine epsilon for float data type using FLT_PSILON is %f\n",x2);

printf("\n");
printf("\n");

double x3=1; 
while(1+x3!=1){x3/=2;} x3*=2;

double x4= FLT_EPSILON;

printf("Machine epsilon for double date type using do while is %lf\n",x3);
printf("Machine epsilon for double data type using FLT_PSILON is %lf\n",x4);


printf("\n");
printf("\n");

long double x5=1; 
while(1+x5!=1){x5/=2;} x5*=2;

long double x6= FLT_EPSILON;

printf("Machine epsilon for long double date type using do while is %.25Lg\n",x5);
printf("Machine epsilon for long double data type using FLT_PSILON is %.25Lg\n",x6);

printf("\n");
printf("\n");

long double sum2= 0.0;
for (int l2= max; l2>0; l2--)
{
sum2=sum2 + (1.0/l2);
}

printf("Sum of the first series is %.25Lg\n",sum);
printf("Sum of the second series is %.25Lg\n",sum2);






}



