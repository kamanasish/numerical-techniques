#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#define PI 3.14159265

// Differential equation in question
int orbital_equation_of_motion(double phi, const double y[], double yprime[], void* params)
{
	double eps = *(double*) params;
	yprime[0] = y[1];
	yprime[1] = 1 + eps*y[0]*y[0] - y[0];
	return GSL_SUCCESS;
}


int main(int argc, char* argv[])
{
	// Default values for command line input
	double eps = 0, uprime = 0, phi_max = 20*PI, delta_phi = 0.01;

	// Read off values from the command line input
	if(argc - 1 == 4)
	{
		eps = atof(argv[1]);
		uprime = atof(argv[2]);
		phi_max = atof(argv[3])*PI;
		delta_phi = atof(argv[4]);
	}

	// Create odeiv2 system
	gsl_odeiv2_system orbital_system;
	orbital_system.function = orbital_equation_of_motion;
	orbital_system.jacobian = NULL;
	orbital_system.dimension = 2;
	orbital_system.params = (void*) &eps;

	// Create odeiv2 driver
	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	gsl_odeiv2_driver* orbital_driver = gsl_odeiv2_driver_alloc_y_new(&orbital_system, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	// Run & Tabulate!
	double t = 0, y[2] = {1,uprime};
	for(double phi = 0; phi < phi_max; phi += delta_phi)
	{
		int status = gsl_odeiv2_driver_apply(orbital_driver, &t, phi, y);
		printf("%lg \t %lg\n",phi,y[0]);
		if(status != GSL_SUCCESS) fprintf(stderr,"error in application of driver\n");
	}

	// Clean the workspace
	gsl_odeiv2_driver_free(orbital_driver);

	return 0;
}
